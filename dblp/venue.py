import logging
import re
import requests
from urllib.request import urlopen
import json
import time
import pdb

from lxml import html
from dblp.paper import Paper
from bs4 import BeautifulSoup

logger = logging.getLogger("dblp-retriever_logger")


class Venue(object):
    """ A venue on DBLP. """

    def __init__(self, name, year, identifier):
        self.name = str(name)
        self.year = str(year)
        self.identifier = str(identifier)

        self.uri = "https://dblp.uni-trier.de/db/" + self.identifier + ".html"

        self.papers = []
        self.bib_entries = []

        # session for data retrievalpdb.set_trace()
        self.session = requests.Session()

    def getAbstract(self, DOIlink, title=''):
        time.sleep(2)
        """ Abstract retrieved from DOI link. might be empty """
        abstract = ""
        # usenix papers need to be searched by title
        if title != '':
            titleToSearch = title.lower()
            titleToSearch = re.sub("[^0-9a-zA-Z ]+", " ", titleToSearch)
            titleToSearch = ' '.join(titleToSearch.split())
            titleToSearch = titleToSearch.strip()
            titleToSearch = titleToSearch.replace(" ","+")

            response = urlopen('https://api.semanticscholar.org/graph/v1/paper/search?query='+titleToSearch+'&fields=title,abstract')
            data_json = json.loads(response.read())
            abstract = data_json["data"][0]["abstract"]
            if abstract is None:
                abstract = ""
                logger.warning("No abstract for " + DOIlink)
            if abstract is not None:
                logger.info("Successfully retrieved DOI page " + DOIlink)
            return abstract
        doi = DOIlink.replace('https://doi.org/', '')
        res = requests.get('https://api.semanticscholar.org/' + doi).text
        parsed_html = BeautifulSoup(res, 'html.parser')
        try:
            abstract = parsed_html.findAll('meta', attrs={'name': 'description'}).pop().attrs['content']
            logger.info("Successfully retrieved DOI page " + DOIlink)
        except IndexError:
            logger.warning("No abstract for " + DOIlink)
            abstract = ''
        return abstract

        # responseDOI = self.session.get(DOIlink, headers=headers)
        # # if too many requests, wait and make another request
        # if responseDOI.status_code == 429:
        #     logger.warn("Response 429 while retrieving abstract of " + DOIlink)
        #     time.sleep(int(responseDOI.headers["Retry-After"])+2)
        #     responseDOI = self.session.get(DOIlink)
        # elif responseDOI.ok:
        #     logger.info("Succesfully retrieved DOI page " + DOIlink)
        #     treeDOI = html.fromstring(responseDOI.content.decode('utf-8'))
        #     #Elsevier
        #     if 'content="Elsevier"' in responseDOI.content.decode('utf-8'):
        #         abstract_paragraphs = treeDOI.xpath('//section[@class="abstract"]/div/*')
        #         pdb.set_trace()
        #         for par in abstract_paragraphs:
        #             if par.tag == "p":
        #                 for t in par.xpath('text()'):
        #                     abstract += t
        #     # Springer
        #     head_metadata = treeDOI.xpath('//head/meta[@name][2]')
        #     if "Springer" in responseDOI.content.decode('utf-8'):
        #         abstract_paragraphs = treeDOI.xpath('//section[@class="Abstract"]/*')
        #         #pdb.set_trace()
        #         # springer journals are different...
        #         if len(abstract_paragraphs) == 0:
        #             abstract_paragraphs = treeDOI.xpath('//section[@data-title="Abstract"]/*')
        #         for par in abstract_paragraphs:
        #             if par.tag == "p":
        #                 for t in par.xpath('text()'):
        #                     abstract += t
        #
        #         return abstract
        #
        #     head_metadata = treeDOI.xpath('//head/meta[@content]')
        #     if len(head_metadata) == 0:
        #         logger.warn("Could not get head metadata while retrieving the DOI link")
        #         return abstract
        #     # ACM DL
        #     if "ACM" in head_metadata[0].attrib.get("content"):
        #         abstract_paragraphs = treeDOI.xpath('//div[@class="abstractSection abstractInFull"]/*')
        #         for par in abstract_paragraphs:
        #             if par.tag == "p":
        #                 for t in par.xpath('text()'):
        #                     abstract += t
        #         return abstract
        #     # IEEE
        #     if "IEEE" in head_metadata[0].attrib.get("content"):
        #         # I wish this was proper json.. it is not really
        #         json_content = re.search('xplGlobal.document.metadata=(.*)};.*</script>.*<div class="ng2-app"', str(responseDOI.content))
        #         if json_content is None:
        #             return ""
        #         json_content = json_content.group(1)+"}".strip()
        #         abstract = re.search('"abstract":"true",.*"abstract":"(.*?)",".*"abstract":"true"', json_content)
        #         if abstract is None:
        #             return ""
        #         else:
        #             return abstract.group(1)
        #         return abstract
        #     else:
        #         return abstract
        # else:
        #     logger.warn(str(DOIlink) + "not retrieved. Error number:" + str(responseDOI.status_code))
        #     return abstract

    def retrieve_papers(self):
        try:
            # retrieve data
            response = self.session.get(self.uri)

            # if too many requests, wait and make another request
            if response.status_code == 429:
                time.sleep(int(response.headers["Retry-After"])+2)
                response = self.session.get(self.uri)
            if response.ok:
                logger.info("Successfully retrieved TOC of venue: "
                            + str(self))

                tree = html.fromstring(response.content)
                items = tree.xpath('//header[not(@class)]/h2 | //header[not(@class)]/h3 | //ul[@class="publ-list"]/li')

                current_heading = ""
                for item in items:
                    if item.tag == "h2" or item.tag == "h3":
                        heading = item.xpath("descendant-or-self::*/text()")
                        if len(heading) > 0:
                            current_heading = re.sub(r'\s+', ' ',  # unify whitespaces/ remove newlines
                                                     str(" ".join(str(element).strip() for element in heading)))
                        else:
                            current_heading = ""
                    elif item.tag == "li":

                        paper_id = item.xpath('@id')[0]
                        title = item.xpath('cite[@class="data tts-content"]/span[@itemprop="name"]/descendant-or-self::*/text()')
                        if len(title) > 0:
                            title = str(" ".join(str(element).strip() for element in title))
                        else:
                            title = ""

                        pages = item.xpath('cite[@class="data tts-content"]/span[@itemprop="pagination"]/text()')
                        if len(pages) > 0:
                            pages = str(pages[0])
                        else:
                            pages = ""

                        ee = item.xpath('nav[@class="publ"]/ul/li[@class="drop-down"]/div[@class="head"]/a/@href')
                        if len(ee) > 0:
                            abstract = ""
                            # select DOI link if available, first link otherwise
                            doi_link = [link for link in ee if "doi.org" in link]                            
                            if len(doi_link) > 0:
                                ee = str(doi_link[0])
#                                if withAbstract:
                                abstract = self.getAbstract(ee)
                            else:
                                ee = str(ee[0])
                                attempts = 0
                                short_title = title
                                while abstract == "" and attempts < 5:
                                    try:
                                        abstract = self.getAbstract(ee, short_title)
                                    except IndexError:
                                        # FIXME: some titles give problems so we try to remove words and search again
                                        #  for the shorter title. Workaround that works sometimes, but a better fix is
                                        #  required.
                                        short_title = short_title.rsplit(' ', 1)[0]
                                        logger.error("attempt "+str(attempts)+": Weird index error for "+short_title)
                                    finally:
                                        attempts = attempts+1
                        else:
                            ee = ""

                        authors = item.xpath('cite[@class="data tts-content"]/span[@itemprop="author"]/a/span[@itemprop="name"]'
                                             '/text()')

                        # if there are no authors it means that it is
                        # the first entry of he proceedings, which
                        # should be skipped.
                        if len(authors) == 0:
                            continue
                        if len(authors) == 1:
                            authors = str(authors[0])
                        else:
                            authors = "; ".join(authors)
                        self.papers.append(Paper(
                            self.name,
                            self.year,
                            self.identifier,
                            current_heading,
                            paper_id,
                            title,
                            authors,
                            pages,
                            ee,
                            abstract
                        ))

                logger.info("Successfully parsed TOC of venue: " + str(self))
            else:
                logger.error("An error occurred while retrieving TOC of venue: "
                             + str(self.uri))

        except ConnectionError:
            logger.error("An error occurred while retrieving TOC of venue: "
                         + str(self))

    def retrieve_venue_bibtex(self):
        logger.info("Retrieving bibtex for venue: " + str(self))
        for paper in self.papers:
            bibtex = paper.get_bibtex()
            self.bib_entries.append(bibtex)

    def validate_page_ranges(self):
        logger.info("Sorting papers of venue: " + str(self))

        self.papers.sort(key=lambda p: p.first_page)

        logger.info("Validating page ranges of venue: " + str(self))

        if len(self.papers) < 2:
            return

        previous_paper = self.papers[0]
        for i in range(1, len(self.papers)):
            current_paper = self.papers[i]

            if current_paper.page_range == "" or previous_paper.page_range == "":
                previous_paper = self.papers[i]
                continue

            if current_paper.regular_page_range and current_paper.first_page != previous_paper.last_page + 1:
                current_paper.append_comment("issue_first_page")
                previous_paper.append_comment("issue_last_page")
                logger.warning("First page of paper " + str(current_paper)
                               + " does not match previous paper "
                               + str(previous_paper))

            elif current_paper.numbered_page_range and current_paper.article_number != previous_paper.article_number + 1:
                current_paper.append_comment("issue_article_number")
                previous_paper.append_comment("issue_article_number")
                logger.warning("Article number of paper "
                               + str(current_paper)
                               + " does not match previous paper "
                               + str(previous_paper))

            previous_paper = self.papers[i]

    def get_rows(self):
        rows = []
        for paper in self.papers:
            rows.append(paper.get_column_values())
        return rows

    def __str__(self):
        return str(self.identifier)
